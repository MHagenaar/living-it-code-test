package domain;

interface IWordCount {
    int getCount();
    String getWord();
}