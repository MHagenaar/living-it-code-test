package domain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        WordCounterAnalyzer wordCounterAnalyzer = new WordCounterAnalyzer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("Enter text: ");
            String name = reader.readLine(); // Read a line of text from the console
            
            System.out.print("Highest wordCount: ");
            int highestWordCount = 0;
            highestWordCount = wordCounterAnalyzer.CalculateHighestWordCount(name);
            System.out.print(highestWordCount);
            System.out.print("\n");

            System.out.print("\n");
            System.out.print("Enter word in text: ");
            String word = reader.readLine();
            
            int wordCount = wordCounterAnalyzer.CalculateWordCount(name,word);
            System.out.print("WordCount: ");
            System.out.print(wordCount);
            System.out.print("\n");

            System.out.print("\n");
            System.out.print("top words:");
            int top = Integer.parseInt(reader.readLine());
            
            List<IWordCount> counter = wordCounterAnalyzer.GetMostCountedWords(name,top);
            int count = 0;
            String wordLoc = "";
            for(int index = 0; index<top; index++) {
                count =  counter.get(index).getCount();
                wordLoc = counter.get(index).getWord();
                System.out.print(wordLoc + ":" + count);
                System.out.print("\n");
            }

        } catch (IOException e) {
            System.err.println("Error reading input: " + e.getMessage());
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }        
    }
}
