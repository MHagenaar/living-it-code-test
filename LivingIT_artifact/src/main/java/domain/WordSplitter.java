package domain;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordSplitter{

    public List<String> StringSplitter(String _text) {
        _text = _text.toLowerCase();
        String[] seperator = {",","."," ","\n"};
        String combinedDelimiter = Stream.of(seperator)
                                         .map(d -> "\\" + d) 
                                         .collect(Collectors.joining("|"));
        List<String> textList = Arrays.stream(_text.split(combinedDelimiter))
                                       .toList();
        return textList;
    }


}