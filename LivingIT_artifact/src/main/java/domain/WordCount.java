package domain;

class WordCount implements IWordCount {
    private int count;
    private String word;

    WordCount(int count,String word) {
        this.count = count;
        this.word = word;
    }

    public int getCount() {
        return this.count;
    }
    public String getWord() {
        return this.word;
    }

}