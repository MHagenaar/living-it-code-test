package domain;

interface IWordCountAnalyzer {
    int CalculateHighestWordCount(String text);
    int CalculateWordCount (String text, String word);
    Object GetMostCountedWords(String text, int top);



}