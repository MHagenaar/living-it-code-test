package domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


class WordCounterAnalyzer implements IWordCountAnalyzer {
    private WordSplitter wordsplitter;

    WordCounterAnalyzer() {
       this.wordsplitter = new WordSplitter(); 
    }

    

    public int CalculateHighestWordCount(String text) {
        List<String> splittedString = wordsplitter.StringSplitter(text);
        int wordCount = splittedString.stream()
                      .map(x -> x.length())
                      .sorted(Comparator.reverseOrder())
                      .toList().get(0);
        return wordCount; 
    }

    public int CalculateWordCount(String text, String word) {
        List<String> splittedString = wordsplitter.StringSplitter(text);
        int wordCount = splittedString.stream()
                                      .filter(n -> word.toLowerCase().equals(n))
                                      .toList()
                                      .size();
    return wordCount;
    }

    public List<String> NumericAlphabeticOrder(String _text)  {
        List<String> listWrapper = new ArrayList<String>(wordsplitter.StringSplitter(_text));
        Collections.sort(listWrapper);
        Collections.sort(listWrapper,Collections.reverseOrder(Comparator.comparingInt(x -> CalculateWordCount(_text,x))));      
        return listWrapper;
    }

    public List<IWordCount> WordSelector(List<String> _wordList,String _text, int _top) {
        List<IWordCount> _wordCount = new ArrayList<IWordCount>();
        String wordPrev = "";
        String word;

        int endIndex = _top;
        for (int i = 0; i<endIndex; i++) {
             word = _wordList.get(i);
             if (!word.equals(wordPrev)) {
                _wordCount.add(new WordCount(CalculateWordCount(_text,word),word));
             }
             else {
                endIndex+=1;
             };
             wordPrev = word;
        }
        return _wordCount;
    }


    public List<IWordCount> GetMostCountedWords(String _text, int _top) {
        List<String> listWrapper = new ArrayList<String>(wordsplitter.StringSplitter(_text));
        listWrapper = NumericAlphabeticOrder(_text);
        return WordSelector(listWrapper,_text,_top);
    }   


}