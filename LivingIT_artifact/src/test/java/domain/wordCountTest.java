package domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class wordCountTest 
{

    @Test
        public void initialize() {
        WordCounterAnalyzer myTestWord = new WordCounterAnalyzer();
        assertNotNull(myTestWord);
    };

    @Test
    public void WordSplitExists()
    {
        WordSplitter myTestWordObject = new WordSplitter();
        List<String> myTestWordList = myTestWordObject.StringSplitter("my,word");
        assertEquals(myTestWordList.get(0),"my");
        assertEquals(myTestWordList.get(1),"word");
    };

    @Test
    public void WordSplitExistsWithMultipleSeperators()
    {
        WordSplitter myTestWordObject = new WordSplitter();
        List<String> myTestWordList = myTestWordObject.StringSplitter("my,word\nIs seperated..");
        assertEquals(myTestWordList.get(0),"my");
        assertEquals(myTestWordList.get(1),"word");
        assertEquals(myTestWordList.get(2),"is");
        assertEquals(myTestWordList.get(3),"seperated");
    };

    @Test
    public void TestHighestWrodCountDifferentWordCount()
    {
        WordCounterAnalyzer myTestWordObject = new WordCounterAnalyzer();
        int myTestWordNumber = myTestWordObject.CalculateHighestWordCount("my,word");
        assertEquals(myTestWordNumber,4);
    };

    @Test
    public void TestHighestWrodCountSameWordCount()
    {
        WordCounterAnalyzer myTestWordObject = new WordCounterAnalyzer();
        int myTestWordNumber = myTestWordObject.CalculateHighestWordCount("word,text");
        assertEquals(myTestWordNumber,4);
    };


    @Test
    public void AreWordsThatAreAllDifferentCalculated()
    {
        WordCounterAnalyzer myTestWordObject = new WordCounterAnalyzer();
        int myTestWordNumber = myTestWordObject.CalculateWordCount("my,word word2","word");
        assertEquals(myTestWordNumber,1);
    };

    @Test
    public void AreTheNumberOFWordsCalculatedWithDoubleWord()
    {
        WordCounterAnalyzer myTestWordObject = new WordCounterAnalyzer();
        int myTestWordNumber = myTestWordObject.CalculateWordCount("my,word word","word");
        assertEquals(myTestWordNumber,2);
    };

    @Test
    public void IsTheCollectionOrdered() {
        WordCounterAnalyzer myTestWordObject = new WordCounterAnalyzer();
        List<String> topWordSet = myTestWordObject.NumericAlphabeticOrder("myText,has,highest number AND\nhas someDuplications");
        assertEquals(topWordSet.get(0),"has");
        assertEquals(topWordSet.get(1),"has");    
        assertEquals(topWordSet.get(2),"and");
        assertEquals(topWordSet.get(3),"highest");    
    }

    @Test
    public void WordIsBeingSelected() {
        WordCounterAnalyzer myTestWordObject = new WordCounterAnalyzer();
        String text = "has,has,and,highest,myText,number,someDuplications";
        List<String> textList = new ArrayList<String>();
        textList.add("has");
        textList.add("has");
        textList.add("and");
        textList.add("highest");
        textList.add("myText");
        textList.add("number");
        textList.add("someDuplications");
        List<IWordCount> resultList = myTestWordObject.WordSelector(textList,text, 3);
        assertEquals(resultList.get(0).getWord(),"has");
        assertEquals(resultList.get(1).getWord(),"and");
        assertEquals(resultList.get(2).getWord(),"highest");
    }

    @Test
    public void AreTheTopTwoWordsCounted()
    {
        WordCounterAnalyzer myTestWordObject = new WordCounterAnalyzer();
        List<IWordCount> topWordSet = myTestWordObject.GetMostCountedWords("myText,Is.has,highest number AND\nsomeDuplications,IS",2);
        assertEquals(topWordSet.get(0).getWord(),"is");
        assertEquals(topWordSet.get(1).getWord(),"and");

    };
    
};


